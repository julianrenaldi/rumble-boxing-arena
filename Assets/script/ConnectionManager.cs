using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using System.IO;
using UnityEngine.SceneManagement;
using MessagePack;
using MessagePack.Resolvers;

public class ConnectionManager : Singleton<ConnectionManager>, RealTimeMultiplayerListener
{
    public PlayGamesClientConfiguration config;
    public Participant player;
    public List<Participant> participants = new List<Participant>();
    public string[] dataReceived;

    protected ConnectionManager() {
        
    }

    public void Init() {
        config = new PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .Build();
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
    }

    public void CreateQuickGame()
    {
        const int MIN_OPPONENTS = 1, MAX_OPPONENTS = 1;
        const int GameType = 0;

        PlayGamesPlatform.Instance.RealTime.CreateQuickGame(MIN_OPPONENTS, MAX_OPPONENTS, GameType, this);
    }

    public void CreateWithInvitationScreen()
    {
        const int MIN_OPPONENTS = 1, MAX_OPPONENTS = 4;
        const int GameType = 0;

        PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(MIN_OPPONENTS, MAX_OPPONENTS, GameType, this);
    }

    

    public void OnRoomSetupProgress(float progress)
    {
       
    }

    public void OnRoomConnected(bool success)
    {
        if (success)
        {
            player = PlayGamesPlatform.Instance.RealTime.GetSelf();
            participants = PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();
            //StartCoroutine(ShopMenuManager.Instance.loadAsync());
            SceneManager.LoadScene(2);
        }
        else
        {
            //Warning error
        }

    }
    

    public void OnLeftRoom()
    {
        throw new NotImplementedException();
    }

    public void OnParticipantLeft(Participant participant)
    {
        participants.Remove(participant);
    }

    public void OnPeersConnected(string[] participantIds)
    {
        throw new NotImplementedException();
    }

    public void OnPeersDisconnected(string[] participantIds)
    {
        throw new NotImplementedException();
    }

    public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
        dataReceived = MessagePackSerializer.Deserialize<string[]>(data, ContractlessStandardResolver.Instance);
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Gameplay") && dataReceived != null)
        {
            GameManager.Instance.txtDebugger.text =
             "SelfID: " + player.ParticipantId +
             "\nParticipant 1: " + participants[0].ParticipantId +
             "\nParticipant 2: " + participants[1].ParticipantId +
             "\nMessage: " + dataReceived[0] +
             "\nPlayer ID: " + dataReceived[1];
        }
    }
}